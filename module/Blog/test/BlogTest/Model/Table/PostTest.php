<?php

namespace BlogTest\Model\Table;

use Blog\Model\Table\Post as PostTable;
use Blog\Model\Post as PostEntity;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class PostTest extends PHPUnit_Framework_TestCase
{

    public function testFetchAllShouldReturnAllPosts()
    {
        $resultSet = new ResultSet();
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', 
            array('selectWith'), array(), '', false);
        $mockTableGateway->expects($this->once())
            ->method('selectWith')
            ->with($this->isInstanceOf('Zend\Db\Sql\Select'))
            ->will($this->returnValue($resultSet));

        $albumTable = new PostTable($mockTableGateway);

        $this->assertSame($resultSet, $albumTable->fetchAll());
    }

    public function testSavePostWillInsertNewPost()
    {
        $postData  = [
            'id'     => 1234,
            'name' => 'somebody',
            'email'  => 'somebody@test.com',
            'text'  => 'bla bla bla',
        ];
        $post = new PostEntity();
        $post->exchangeArray($postData);

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('insert'), array(), '', false);
        $mockTableGateway->expects($this->once())
            ->method('insert')
            ->with($postData);

        $albumTable = new PostTable($mockTableGateway);
        $albumTable->save($post);
    }

}
