<?php

namespace BlogTest\Model;

use Blog\Model\Post;
use PHPUnit_Framework_TestCase;

class PostTest extends PHPUnit_Framework_TestCase
{
    public function testPostStateShouldBeNullWhenInstanceIsCreated()
    {
        $post = new Post();

        $this->assertNull($post->id);
        $this->assertNull($post->name);
        $this->assertNull($post->email);
        $this->assertNull($post->text);
    }

    public function testExchangeArrayShouldSetsPropertiesCorrectly()
    {
        $post = new Post();
        $data  = [
            'id'     => 1234,
            'name' => 'somebody',
            'email'  => 'somebody@test.com',
            'text'  => 'bla bla bla',
        ];

        $post->exchangeArray($data);

        $this->assertSame($data['id'], $post->id);
        $this->assertSame($data['name'], $post->name);
        $this->assertSame($data['email'], $post->email);
        $this->assertSame($data['text'], $post->text);
    }

    public function testExchangeArrayShouldSetsPropertiesToNullIfKeysNotExist()
    {
        $post = new Post();

        $data  = [
            'id'     => 123,
            'name' => 'somebody',
            'email'  => 'somebody@test.com',
            'text'  => 'bla bla bla',
        ];
        $post->exchangeArray($data);
        $post->exchangeArray([]);

        $this->assertNull($post->id);
        $this->assertNull($post->name);
        $this->assertNull($post->email);
        $this->assertNull($post->text);
    }
}