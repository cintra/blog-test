<?php

namespace BlogTest\Filter;

use Blog\Filter\HiperlinkParser;

class HiperlinkParserTest extends \PHPUnit_Framework_TestCase
{
    public function testShouldCreateHiperlinkWhenUrlIsFound()
    {
        $expected = 'visit <a href="http://www.goodgamestudios.com" target="_blank">'
            .'www.goodgamestudios.com</a> and enjoy yourself.'.PHP_EOL
            .'A nice game to try is <a href="http://empire.goodgamestudios.com/" target="_blank">http://empire.goodgamestudios.com/</a>';
        $text = 'visit www.goodgamestudios.com and enjoy yourself.'.PHP_EOL
            .'A nice game to try is http://empire.goodgamestudios.com/';
        $filter = new HiperlinkParser();
        $this->assertEquals($expected, $filter->filter($text));
    }
}
