<?php
namespace Blog;

use Blog\Model\Post as PostEntity;
use Blog\Model\Table\Post as PostTable;
use Blog\Model\Comment as CommentEntity;
use Blog\Model\Table\Comment as CommentTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Blog\Model\Table\Post' =>  function($sm) {
                    $tableGateway = $sm->get('PostTableGateway');
                    $table = new PostTable($tableGateway);
                    return $table;
                },
                'PostTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PostEntity());
                    return new TableGateway('post', $dbAdapter, null, $resultSetPrototype);
                },
                'Blog\Model\Table\Comment' =>  function($sm) {
                    $tableGateway = $sm->get('CommentTableGateway');
                    $table = new CommentTable($tableGateway);
                    return $table;
                },
                'CommentTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CommentEntity());
                    return new TableGateway('comment', $dbAdapter, null, $resultSetPrototype);
                },

            ),
            'instance' => array(
                'Zend\View\Resolver\AggregateResolver' => array(
                    'injections' => array(
                        'Zend\View\Resolver\TemplateMapResolver',
                        'Zend\View\Resolver\TemplatePathStack',
                    ),
                ),
                'Zend\View\Resolver\TemplateMapResolver' => array(
                    'parameters' => array(
                        'map'  => array(
                            'layout'      => __DIR__ . '/view/layout.phtml',
                            'index/index' => __DIR__ . '/view/index/index.phtml',
                        ),
                    ),
                ),
                'Zend\View\Resolver\TemplatePathStack' => array(
                    'parameters' => array(
                        'paths'  => array(
                            'application' => __DIR__ . '/view',
                        ),
                    ),
                ),
                'Zend\View\Renderer\PhpRenderer' => array(
                    'parameters' => array(
                        'resolver' => 'Zend\View\Resolver\AggregateResolver',
                    ),
                ),
            )
        );
    }

}
