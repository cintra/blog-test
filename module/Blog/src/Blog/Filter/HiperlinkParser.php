<?php

namespace Blog\Filter;

use Zend\Filter\FilterInterface;

class HiperlinkParser implements FilterInterface
{
    public function filter($value)
    {
        $value = $this->createHiperlink($value);
        return $this->formatUrl($value);
    }
    
    private function createHiperlink($text)
    {
        $regex = '/(((http|https):\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}'.'((:\d{1,5})?\/.*)?)/i';
        return preg_replace($regex, '<a href="$1" target="_blank">$1</a>', $text);
    }
    
    private function formatUrl($text)
    {
        return preg_replace('/<a href="(?!https?:\/\/)/', '<a href="http://', $text);
    }
    
}
