<?php

namespace Blog\Filter;

use Zend\Filter\FilterInterface;

class BreakLineToTag implements FilterInterface
{
    public function filter($value)
    {
        return nl2br($value);
    }
        
}
