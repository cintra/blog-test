<?php
namespace Blog\Form;

class Comment extends Base
{
    
    public function __construct($name = 'comment')
    {
        parent::__construct($name);
        $this->add(array(
            'name' => 'fk_post',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
    }
        
}
