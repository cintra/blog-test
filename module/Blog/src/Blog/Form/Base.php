<?php
namespace Blog\Form;

use Zend\Form\Form;

abstract class Base extends Form
{
    public function __construct($name = 'post')
    {
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        
        $this->addId();
        $this->addName();
        $this->addEmail();
        $this->addText();
        $this->addCaptcha();
    }
    
    protected function addId()
    {
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
    }
    
    protected function addName()
    {
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Author Name',
            ),
        ));
    }


    protected function addEmail()
    {
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'email',
            ),
            'options' => array(
                'label' => 'E-mail',
            ),
        ));
    }


    protected function addText()
    {
        $this->add(array(
            'name' => 'text',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
                'label' => 'Text',
            ),
        ));
    }
    
    protected function addCaptcha()
    {
        $this->add(array(
            'type' => 'Zend\Form\Element\Captcha',
            'name' => 'captcha',
            'options' => array(
                'captcha' => array(
                    'class' => 'Dumb',
                ),
            ),
        ));
    }
        
}
