<?php

namespace Blog\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Blog\Filter\HiperlinkParser;
use Blog\Filter\BreakLineToTag;

class Comment extends BaseEntity
{

    public $id;
    public $name;
    public $email;
    public $text;
    public $fk_post;

    public function exchangeArray(array $data)
    {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->name = (isset($data['name'])) ? $data['name'] : null;
        $this->email = (isset($data['email'])) ? $data['email'] : null;
        $this->text = (isset($data['text'])) ? $data['text'] : null;
        $this->fk_post = (isset($data['fk_post'])) ? $data['fk_post'] : null;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' =>$this->email,
            'text' => $this->text,
            'fk_post' => $this->fk_post,
        ];
    }
    
    public function getInputFilter()
    {
        parent::getInputFilter();
        return $this->inputFilter->add((new InputFactory())->createInput(array(
            'name'     => 'fk_post',
            'required' => true,
            'filters'  => array(
                array('name' => 'Int'),
            ),
        )));
    }
    
}