<?php

namespace Blog\Model\Table;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class Post
{
    private $table;

    public function __construct(TableGateway $tableGateway)
    {
        $this->table = $tableGateway;
    }

    public function fetchAll()
    {
        $select = new Select();
        $select->from("post")
            ->order('id DESC');
        $resultSet = $this->table->selectWith($select);
        return $resultSet;
    }

    public function fetchById($id)
    {
        $rowset = $this->table->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * @param \Blog\Model\Post $entity
     * @return \Blog\Model\Post|boolean
     */
    public function save(\Blog\Model\Post $entity)
    {
        try {
            $this->table->insert($entity->toArray());
            $entity->id = (int) $this->table->getLastInsertValue();
        } catch (\Exception $e) {
            return false;
        }
        return $entity;
    }

}