<?php

namespace Blog\Model\Table;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class Comment
{
    private $table;

    public function __construct(TableGateway $tableGateway)
    {
        $this->table = $tableGateway;
    }

    public function fetchAll($id)
    {
        return $this->table->select(array('fk_post' => $id));
    }

    public function save(\Blog\Model\Comment $entity)
    {
        try {
            $this->table->insert($entity->toArray());
            $entity->id = (int) $this->table->getLastInsertValue();
        } catch (\Exception $e) {
            return false;
        }
        return $entity;
    }

}