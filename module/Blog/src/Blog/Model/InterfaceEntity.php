<?php

namespace Blog\Model;

interface InterfaceEntity
{

    /**
     * @param array $data
     * @return mixed
     */
    public function exchangeArray(array $data);

    public function toArray();
}