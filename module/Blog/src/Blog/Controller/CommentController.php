<?php

namespace Blog\Controller;

use Zend\View\Model\ViewModel;
use Blog\Model\Comment as CommentEntity;
use Blog\Form\Comment as CommentForm;

class CommentController extends BaseController
{

    public function formAction()
    {
        $post_id = (int) $this->params()->fromRoute('post_id', 0);
        $form = new CommentForm();
        $form->setData(['fk_post' => $post_id]);
        $view = new ViewModel(['form' => $form]);
        if ($this->getRequest()->isXmlHttpRequest()) {
            $view->setTerminal(true);
        }
        return $view;
    }

    public function addAction()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->setFailureResponse('Form Data is missing.');
        }

        $form = new CommentForm();
        $entity = new CommentEntity();
        $form->setInputFilter($entity->getInputFilter());
        $form->setData($this->getRequest()->getPost());

        if ($form->isValid()) {
            $entity->exchangeArray($form->getData());
            $entity = $this->getCommentTable()->save($entity);
            if (false !== $entity) {
                $content = $this->renderPartial('blog/comment/partial/comment.phtml', ['comment' => $entity]);
                return $this->setSuccessResponse($content);
            } else {
                $message = 'Error on save post';
            }
        } else {
            $message = $form->getMessages();
        }
        return $this->setFailureResponse($message);
    }

}
