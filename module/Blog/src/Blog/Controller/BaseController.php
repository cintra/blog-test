<?php

namespace Blog\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Renderer\PhpRenderer;
use Blog\Model\Post as PostEntity;
use Blog\Form\Post as PostForm;

abstract class BaseController extends AbstractActionController
{
    /**
     * @var type Blog\Model\Table\Comment
     */
    protected $commentTable;

    /**
     * @return Blog\Model\Table\Comment
     */
    public function getCommentTable()
    {
        if (null === $this->commentTable) {
            $this->commentTable = $this->getServiceLocator()
                ->get('Blog\Model\Table\Comment');
        }
        return $this->commentTable;
    }

    /**
     * Return a rendered partial view
     * 
     * @param string $template
     * @param array $data
     * @return string
     */
    protected function renderPartial($template, array $data)
    {
        $renderer = $this->getServiceLocator()->get('Zend\View\Renderer\PhpRenderer');
        return $renderer->partial($template, $data);
    }

    /**
     * @param string|array $message
     * @return json
     */
    protected function setFailureResponse($message = '')
    {
        $response = [
            'isSuccess' => false,
            'message' => $message
        ];
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($response));
    }

    /**
     * @param string $content
     * @return json
     */
    protected function setSuccessResponse($content = '', $order = 'asc')
    {
        $response = [
            'isSuccess' => true,
            'content' => $content,
            'order' => $order,
        ];
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($response));
    }

}
