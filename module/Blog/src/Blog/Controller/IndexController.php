<?php
namespace Blog\Controller;

use Zend\View\Model\ViewModel;
use Blog\Model\Post as PostEntity; 
use Blog\Form\Post as PostForm;

class IndexController extends BaseController
{
    private $postTable;
    
    public function getPostTable()
    {
        if (null === $this->postTable) {
            $this->postTable = $this->getServiceLocator()
                ->get('Blog\Model\Table\Post');
        }
        return $this->postTable;
    }
        
    public function indexAction()
    {
        return new ViewModel(array(
            'posts' => $this->getPostTable()->fetchAll(),
        ));
    }

    public function formAction()
    {
        $form = new PostForm();
        $view = new ViewModel(array('form' => $form));
        if ($this->getRequest()->isXmlHttpRequest()) {
            $view->setTerminal(true);
        }
        return $view;
    }

    public function addAction()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->setFailureResponse('Form Data is missing.');
        }

        $form = new PostForm();
        $entity = new PostEntity();
        $form->setInputFilter($entity->getInputFilter());
        $form->setData($this->getRequest()->getPost());

        if ($form->isValid()) {
            $entity->exchangeArray($form->getData());
            $entity = $this->getPostTable()->save($entity);
            if (false !== $entity) {
                $content = $this->renderPartial('blog/index/partial/post.phtml', array('post' => $entity));
                return $this->setSuccessResponse($content, 'desc');
            } else {
                $message = 'Error on save post';
            }
        } else {
            $message = $form->getMessages();
        }

        return $this->setFailureResponse($message);
    }

    public function postAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('home');
        }
        return new ViewModel(array(
            'post' => $this->getPostTable()->fetchById($id),
            'comments' => $this->getCommentTable()->fetchAll($id),
        ));
    }

}