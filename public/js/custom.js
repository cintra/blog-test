// /public/js/custom.js

function getFormData($form) {
    var serializedArray = $form.serializeArray();
    var newArray = {};

    $.map(serializedArray, function(item, i) {
        newArray[item['name']] = item['value'];
    });

    return newArray;
}

function cleanForm()
{
    $("#form div").html('');
    $("#form").hide();
    $("#addLayer").show();
    $("#noItem").hide();
}

function parseMessages(message)
{
    if ('string' === typeof message) {
        return message;
    }

    messages = new Array();
    for (var key in message) {
        parsedMessage = parseMessages(message[key]);
        messages.push(key + ": " + parsedMessage);
    }
    return messages.join("\n");
}

jQuery(function($) {
    $("#add").on('click', function(event) {
        event.preventDefault();
        $.get($("#formUrl").val(), null, function(data) {
            $("#form div").html(data);
            $("#form").show();
            $("#addLayer").hide();
        });
    });

    $("#savebutton").on('click', function(event) {
        event.preventDefault();
        var data = getFormData($("#form fieldset"));
        $.post($("#saveUrl").val(), data, function(data) {
            if (true === data.isSuccess) {
                if ("desc" === data.order) {
                    $("#items").prepend(data.content);
                } else {
                    $("#items").append(data.content);
                }
                cleanForm();
            } else {
                alert(parseMessages(data.message));
            }
        }, 'json');
    });

    $("#cancelbutton").on('click', function(event) {
        cleanForm();
    });

});