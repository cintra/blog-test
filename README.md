SET UP 

To application run, is necessary to do these steps:

1- import the database.sql located in scripts. It will create a database called cintra_blog_test;

Ex:
mysql -uYOUR_USER -pYOUR_PASSWORD < /path/to/blog-test/scripts/database.sql

2- Make a copy from config/autoload/local.php.dist to config/autoload/local.php, so set the user 
and password to property values;

3- Server settings:
3.1- The simplest way to get started if you are using PHP 5.4 or above is to start the internal PHP cli-server in the root directory:

    php -S 0.0.0.0:8080 -t public/ public/index.php

3.2- Or else, you should create a virtual host like the example found on scripts/blog-test.vhost.example
Ex (based on ubuntu systems):
sudo cp /path/to/blog-test/scripts/blog-test.vhost.example /etc/apache2/sites-avaliable/
## so setting the local path, server name etc;

sudo alias -s /etc/apache2/sites-avaliable/blog-test.vhost.example /etc/apache2/sites-enabled/blog-test.vhost.example

sudo /etc/init.d/apache2 restart

#create a host entry:
gedit /etc/hosts
# do add a line like that: "127.0.0.1   blogtest.dev"
